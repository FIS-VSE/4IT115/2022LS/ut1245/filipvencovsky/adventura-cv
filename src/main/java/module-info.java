module cz.vse.java.adventura {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    exports cz.vse.java.adventura.main;
    opens cz.vse.java.adventura.main to javafx.controls, javafx.fxml;
}