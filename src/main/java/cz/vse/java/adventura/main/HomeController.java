package cz.vse.java.adventura.main;

import cz.vse.java.adventura.logika.*;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class HomeController implements Pozorovatel {

    @FXML private ImageView hrac;
    @FXML private ListView<Prostor> panelVychodu;
    @FXML private TextArea vystup;
    @FXML private TextField vstup;
    @FXML private Button odesli;

    private Map<String, Point2D> souradniceProstoru;
    private IHra hra = new Hra();

    @FXML
    private void initialize() {
        zalozSouradniceProstoru();
        hra.getHerniPlan().registruj(this);
        vystup.appendText(hra.vratUvitani()+"\n\n");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                vstup.requestFocus();
            }
        });
        aktualizujPanelVychodu();
        panelVychodu.setCellFactory(listView -> new ProstorListCell());
    }

    private ImageView getObrazekProstoru(Prostor prostor) {
        String cestaObrazky = getClass().getResource("/obrazkyProstoru").toExternalForm();
        ImageView iw = new ImageView(cestaObrazky+prostor.getNazev()+".jpg");
        iw.setFitHeight(70);
        iw.setPreserveRatio(true);
        return iw;
    }

    public void zalozSouradniceProstoru() {
        souradniceProstoru = new HashMap<>();
        souradniceProstoru.put("les", new Point2D(91,31));
        souradniceProstoru.put("domeček", new Point2D(14,79));
        souradniceProstoru.put("hluboký_les", new Point2D(151,79));
        souradniceProstoru.put("chaloupka", new Point2D(225,31));
        souradniceProstoru.put("jeskyně", new Point2D(155,158));
    }

    public Map<String, Point2D> getSouradniceProstoru() {
        if(souradniceProstoru == null) zalozSouradniceProstoru();
        return souradniceProstoru;
    }

    private void aktualizujPoziciHrace() {
        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();
        Point2D souradnice = getSouradniceProstoru().get(aktualniProstor.getNazev());
        hrac.setLayoutX(souradnice.getX());
        hrac.setLayoutY(souradnice.getY());
    }

    private void aktualizujPanelVychodu() {
        panelVychodu.getItems().clear();
        Collection<Prostor> vychody = hra.getHerniPlan().getAktualniProstor().getVychody();
        panelVychodu.getItems().addAll(vychody);
    }

    @FXML
    private void odesliVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        vstup.clear();
        zpracujPrikaz(prikaz);
    }

    private void zpracujPrikaz(String prikaz) {
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText("> "+prikaz+"\n");
        vystup.appendText(vysledek+"\n\n");

        if(hra.konecHry()) {
            vystup.appendText(hra.vratEpilog());
            vstup.setDisable(true);
            odesli.setDisable(true);
            panelVychodu.setDisable(true);
        }
    }

    @Override
    public void aktualizuj(PredmetPozorovani predmetPozorovani) {
        aktualizujPanelVychodu();
        aktualizujPoziciHrace();
    }

    public void klikPanelVychodu(MouseEvent mouseEvent) {
        Prostor novyProstor = panelVychodu.getSelectionModel().getSelectedItem();
        if(novyProstor == null) return;
        String prikaz = PrikazJdi.NAZEV+" "+novyProstor;
        zpracujPrikaz(prikaz);
    }

    public void klikHrac(MouseEvent mouseEvent) {
        zpracujPrikaz(PrikazObsahBatohu.NAZEV);
    }

    public void zobrazNapovedu(ActionEvent actionEvent) {
        Stage stage = new Stage();
        WebView ww = new WebView();
        stage.setScene(new Scene(ww));
        ww.getEngine().load(getClass().getResource("napoveda.html").toExternalForm());
        stage.show();
    }

    class ProstorListCell extends ListCell<Prostor> {
        @Override
        protected void updateItem(Prostor prostor, boolean empty) {
            super.updateItem(prostor, empty);
            if(prostor != null) {
                setText(prostor.getNazev());
                setGraphic(getObrazekProstoru(prostor));
            } else {
                setText(null);
                setGraphic(null);
            }
        }
    }
}
