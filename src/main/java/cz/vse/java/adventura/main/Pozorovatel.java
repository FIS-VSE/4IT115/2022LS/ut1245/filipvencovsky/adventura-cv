package cz.vse.java.adventura.main;

public interface Pozorovatel {
    void aktualizuj(PredmetPozorovani predmetPozorovani);
}
